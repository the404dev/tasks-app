import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Fab} from 'native-base';

import moment from 'moment';
import 'moment/locale/pt-br';
import todayImage from '../../assets/imgs/today.jpg';
import commonStyles from '../commonStyles';
import Task from '../components/Task';
import Icon from 'react-native-vector-icons/FontAwesome';
import AddTask from './AddTask';
import AsyncStorage from '@react-native-community/async-storage';

export default class Schedule extends Component {
  state = {
    tasks: [],
    visibleTaks: [],
    showDoneTasks: true,
    showAddTask: false,
  };

  addTask = task => {
    const tasks = [...this.state.tasks];
    tasks.push({
      id: Math.random(),
      desc: task.desc,
      estimatedAt: task.date,
      doneAt: null,
    });
    this.setState({tasks, showDoneTasks: false}, this.filterTask);
  };

  deleteTask = id => {
    const tasks = this.state.tasks.filter(task => task.id !== id);
    this.setState({tasks}, this.filterTask);
  };

  filterTask = () => {
    let visibleTaks = null;
    if (this.state.showDoneTasks) {
      visibleTaks = [...this.state.tasks];
    } else {
      const pending = task => task.doneAt === null;
      visibleTaks = this.state.tasks.filter(pending);
    }
    this.setState({visibleTaks});
    AsyncStorage.setItem('tasks', JSON.stringify(this.state.tasks));
  };

  toggleFilter = () => {
    this.setState({showDoneTasks: !this.state.showDoneTasks}, this.filterTask);
  };

  componentDidMount = async () => {
    const data = await AsyncStorage.getItem('tasks');
    const tasks = JSON.parse(data) || [];
    this.setState({tasks}, this.filterTask);
    this.filterTask();
  };

  toggleTask = id => {
    const tasks = this.state.tasks.map(task => {
      if (task.id === id) {
        task = {...task};
        task.doneAt = task.doneAt ? null : new Date();
      }
      return task;
    });
    this.setState({tasks}, this.filterTask);
  };
  render() {
    return (
      <View style={styles.container}>
        <AddTask
          isVisible={this.state.showAddTask}
          onSave={this.addTask}
          onCancel={() => this.setState({showAddTask: false})}
        />
        <ImageBackground source={todayImage} style={styles.background}>
          <View style={styles.iconBar}>
            <TouchableOpacity onPress={this.toggleFilter}>
              <Icon
                name={this.state.showDoneTasks ? 'eye' : 'eye-slash'}
                size={20}
                color={commonStyles.colors.secondary}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.titleBar}>
            <Text style={styles.title}>Hoje</Text>
            <Text style={styles.subtitle}>
              {moment()
                .locale('pt-br')
                .format('ddd, D [de] MMMM')}
            </Text>
          </View>
        </ImageBackground>
        <View style={styles.taskContainer}>
          <FlatList
            data={this.state.visibleTaks}
            keyExtractor={item => `${item.id}`}
            renderItem={({item}) => (
              <Task
                {...item}
                toggleTask={this.toggleTask}
                onDelete={this.deleteTask}
              />
            )}
          />
        </View>
        <Fab
          style={{backgroundColor: commonStyles.colors.today}}
          onPress={() => this.setState({showAddTask: true})}
          position="bottomRight">
          <Icon name="plus" />
        </Fab>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 3,
  },
  titleBar: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  title: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 50,
    marginLeft: 20,
    marginBottom: 10,
  },
  subtitle: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 20,
    marginLeft: 20,
    marginBottom: 30,
  },
  taskContainer: {
    flex: 7,
  },
  iconBar: {
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});
